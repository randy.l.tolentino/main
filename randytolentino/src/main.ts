import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import { environment as env} from './environments/environment';
import * as agGridEnterprise from "ag-grid-enterprise"
agGridEnterprise.LicenseManager.setLicenseKey(env.agGridLicense);

if (env.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
