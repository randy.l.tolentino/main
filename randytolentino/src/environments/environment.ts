// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://rltolentino.com/api',
  maintenance: false,
  agGridLicense: "CompanyName=Logistik System Service GmbH_on_behalf_of_Rolls-Royce Deutschland Ltd & Co KG,LicensedGroup=Multi,LicenseType=MultipleApplications,LicensedConcurrentDeveloperCount=1,LicensedProductionInstancesCount=0,AssetReference=AG-011326,ExpiryDate=6_November_2021_[v2]_MTYzNjE1NjgwMDAwMA==250669791af52437e29ada95ec37ca9f"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
