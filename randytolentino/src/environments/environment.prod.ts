export const environment = {
  production: true,
  apiUrl: 'https://rltolentino.com/api',
  maintenance: false
};
