import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse,
  HttpHeaders,
  HttpXsrfTokenExtractor
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry, tap} from 'rxjs/operators'
import {environment as env} from 'src/environments/environment'

@Injectable()
export class HttpBearerInterceptor implements HttpInterceptor {

  constructor(private tokenExtractor: HttpXsrfTokenExtractor) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    console.log('is this even being called?');

    let token = null;
    const xToken = this.tokenExtractor.getToken() as string;

    if (!env.production) {
      token = localStorage["ut"];
    } else {
      token = localStorage["ut"];
    }

    let headers: any = {
      'Content-Type':  'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      //'XSRF-TOKEN': xToken
    };
    if (token) {
      headers = {...headers, ...{'Authorization': `Bearer randy` }};
    }

    request = request.clone({
      setHeaders: headers
    });

    return next.handle(request).pipe(
      tap(evt => {
        if (evt instanceof HttpResponse) {
          if (evt.body) {
            // console.log('**** Interceptor success' , evt.body);
          }
        }
      }),
      retry(1),
      catchError((error: HttpErrorResponse) => {
        let errorMessage = "";
        if (error.error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Error: ${error.error.message}`;
        } else {
          // server-side error
          errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        //window.alert(errorMessage);
        console.log('********HTTP ERROR: *************', errorMessage);
        
        return throwError(errorMessage);
      })
    );
  }
}
