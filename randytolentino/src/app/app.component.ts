import { Component, OnDestroy, OnInit } from '@angular/core';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { SharedService } from 'projects/rtlib/src/lib/shared/shared.service';
import { ShopService } from 'projects/shop/src/app/services/shop.service';
import { AuthService } from './auth/auth.service';
import { environment as env } from 'src/environments/environment';
import { Router } from '@angular/router';
import { LoggedInUser } from 'projects/rtlib/src/public-api';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'randytolentino';
  loggedInUser: LoggedInUser;

  private _subscription = new Subscription();
  
  constructor(
    private _socialAuthService: SocialAuthService,
    private _authService: AuthService,
    private _shopService: ShopService,
    private _router: Router,
    public sharedService: SharedService
    ){
      //this.basket = this.sharedService.basket;
    }

  ngOnInit(){
    const self = this;
    this._subscription.add(this._socialAuthService.initState.subscribe( (w) => {console.log('w', w)}));
    this._subscription.add(this._socialAuthService.authState.subscribe((user) => {
      console.log("user in authState", user)
      if(user && user.id){
          self._subscription.add(self._shopService.login({token: user.authToken}).subscribe(result => {
            console.log("backend login result", result);
            const resultData = result.data || result;
            if(result.data){
              if(user.id==(result.data.socialData || {}).id || env.maintenance){ //can be without
                self._authService.setUserResult(user, result.data.userData);
                //self.loggedInUser = self.sharedService.uu;
              }
            } else {
              console.warn("backend login response error", result);
              self.signout();
            }
          }, error => {
            console.warn("backend login error", error);
            self.signout();
          }));
      } 
    }));
  
    if(this._authService.isLoggedIn()){
      this._authService.setUserResult();
      //this.loggedInUser = this.sharedService.uu;
      return;
    }
  }

  signout(){
    this._authService.signOutAll();
    this.loggedInUser = null;
  }

  manageStore(){
    console.log("loggedInUser", this.loggedInUser)
    this._router.navigate([`seller${ this.loggedInUser && this.loggedInUser.stores && this.loggedInUser.stores.length > 0 ? "/" + this.loggedInUser.stores[0].name : ""}`]);
  }

  signupAsRider(){
    this._router.navigate([`rider`]);
  }

  displayBasketContent(){
    return this.sharedService.basket.map(b => {
      return b.name + " (" +  b.selections.map(bs => bs.value).join(" - ") + ")";
    });
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }
}
