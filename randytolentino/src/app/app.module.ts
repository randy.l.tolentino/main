import { BrowserModule } from '@angular/platform-browser';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RtlibModule } from 'projects/rtlib/src/public-api';
import { EclipseModule } from 'projects/eclipsetea/src/app/app.module';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
  AmazonLoginProvider,
} from 'angularx-social-login';
import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { LoginComponent } from './auth/login/login.component';
import { HttpClient, HttpClientModule, HttpClientXsrfModule, HttpHandler, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ShopAppModule } from 'projects/shop/src/app/app.module';
import { SharedModule } from 'projects/rtlib/src/lib/shared/shared.module';
import { Subject } from 'rxjs';
import { UnauthorizedComponent } from './auth/unauthorized/unauthorized.component';
import { HttpBearerInterceptor } from './services/http-bearer.interceptor';

const loggingIn = new Subject<boolean>();

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UnauthorizedComponent
  ],
  imports: [ 
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SocialLoginModule,
    RtlibModule,
    EclipseModule,
    ShopAppModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'XSRF-TOKEN',
      //headerName: 'X-XSRF-TOKEN',
    }),
    SharedModule.forRoot({
      sharedService: {
        uu: null,
        loggingIn,
        basket: []
      }
    })
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        /* autoLogin: true, */
        providers: [
          /* {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              'clientId'
            ),
          }, */
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('473329730294628'),
          }/* ,
          {
            id: AmazonLoginProvider.PROVIDER_ID,
            provider: new AmazonLoginProvider(
              'clientId'
            ),
          }, */
        ],
      } as SocialAuthServiceConfig,
    },
    { provide: DEFAULT_CURRENCY_CODE, useValue: 'PHP' },
    { provide: LOCALE_ID, useValue: 'en-PH' },
    /* Location, { provide: LocationStrategy, useClass: HashLocationStrategy }, */
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpBearerInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
