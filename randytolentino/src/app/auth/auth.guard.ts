import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private _authService: AuthService,
    private _router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    if (this._authService.isLoggedIn()) {
      return true;
    } else {
      const landedUrl = '' + window.location.pathname;
      if(landedUrl != "/auth/login"){
        console.log('window.location.hash.slice(1)', landedUrl);
        localStorage.setItem("landedurl", landedUrl);
        this._router.navigate(["/auth/login"]);      
      }
      return false;
    }
  }
  
}

