import { Component, OnInit } from '@angular/core';
import { SocialAuthService, GoogleLoginProvider, FacebookLoginProvider, SocialUser } from 'angularx-social-login';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { ShopService } from 'projects/shop/src/app/services/shop.service';
import { SharedService } from 'projects/rtlib/src/public-api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: SocialUser;
  loggedIn: boolean;
  action: 'login' | 'logout';

  constructor(
    private _shopService: ShopService,
    private _socialAuthService: SocialAuthService,
    private _authService: AuthService,
    private _activatedRoute: ActivatedRoute,
    private _sharedService: SharedService,
    private _router: Router ) { 
      if(this._activatedRoute.snapshot.params.action){
        this.action = this._activatedRoute.snapshot.params.action;
      }
    }

  private _signInWithFB(): void {
    if(!localStorage.getItem("uu")){
      console.log("user from localstorage");
      this._sharedService.loggingIn.next(true);
      this._socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID)/* .then(user => {
        console.log("user with token", user);
      }) */;
    } /* else{
      this._authService2.setUserResult(localStorage.getItem("uu"));
    } */
    
  }
  
  signInWithGoogle(): void {
    this._socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
 
  signIn(where?: 'fb' | 'go' | 'am' | 'app' | 'guest'){
    switch(where){
      case 'fb':  this._signInWithFB(); break;
      default: 
        this._sharedService.loggingIn.next(true);
        this._authService.setUserResult({name: 'Randy'}, {});
    }
  }

  testLogin(where){
    if(where=="fb"){
      this._shopService.getLogin({token: "EAAGufapvk2QBANRmhu4LTRwGyGwzfWyZAdlkbTG7FMRsSZCwQJ2jeG59b5fKgvmJb0ZActOzaXmIQXEpDqVRZCXlsZBPFbsnxjs4knswGpUgKgjLiVZAeNdsvo6S9SL9Pi4sZBd8ysJl67c3jhEKLVpjItRNQm1L4yDXnAR1wpZAAmVU8f7EsNfB"})
      .subscribe(result => { 
        console.log("text success", result)
      },err => {
        console.log("and why?", err);
      });
    }
  }

  ngOnInit(): void {
    console.log("this.action", this.action);
    if(this.action == 'logout'){
      this._authService.signOutAll();
    } else if(this.action == 'login' && this._authService.isLoggedIn()){
      console.log("hey sigin", window.location.pathname);
      const landedUrl = localStorage.getItem("landerdUrl");
      const lastUrl = localStorage.getItem("lastUrl");
      if(window.location.pathname == "/auth/login"){
        if((landedUrl && landedUrl != "/auth/login") || (lastUrl && lastUrl != "/auth/login")){
          this._router.navigate([(landedUrl || lastUrl)]);
        } else if(this._sharedService.uu && this._sharedService.uu.lastShop){
          this._router.navigate(["/shop/" + this._sharedService.uu.lastShop.name]);
        } else {
          this._router.navigate(["/shop"]);
        }
      }
    }
  }
}
