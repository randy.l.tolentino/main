import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { SharedService } from 'projects/rtlib/src/public-api';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private _router: Router,
    private _authService: SocialAuthService,
    private _sharedService: SharedService) { 
      this._sharedService.loggingIn.subscribe(l => {
        this._loggingIn = l
      });
    }

    private _loggingIn: boolean;

    setUserResult(u?, data?): any {
      console.log("setting user", u);
      let user = u || JSON.parse(localStorage.getItem("uu"));
      
      if(data){
        // combine user and backend results
        user = {...user, ...data};
      }
      /* this._sharedService.loggedInUser.next({...this._sharedService.loggedInUser, ...user}); */
      this._sharedService.uu = user;

      if(u){
        localStorage.setItem("uu", JSON.stringify(user));
      }
      
      /* this._loggingIn = true; */
      if(this._loggingIn){
        let page = ""
        if(user.isAdmin){
          page = "/admin"
        } else if(user.isRider){
          page = "/rider";
        } else if(user.isSeller){
          page = "/seller";
        } else {
          page = "/shop";
        }
        if(page){
          if(!user.isAdmin){
            if(user.lastShop){
              page += `/${user.lastShop.name}`;
            }
          }
          console.log("goto " +page);
          this.goTo(page);
        } else{
          if(localStorage.getItem("landedurl") && localStorage.getItem("landedurl") != "/auth/login"){
            this.goTo(localStorage.getItem("landedurl"));
          } else if(localStorage.getItem("lasturl") && localStorage.getItem("lasturl") != "/auth/login"){
            this.goTo(localStorage.getItem("lasturl"));
          }
        }
        this._sharedService.loggingIn.next(false);
      } /* else{
        this.goTo("/shop");
      } */

      return u;
    }
    
  signOutAll(){
    console.log("logging out, TODO: check if FB logged in");
    this._authService.signOut();
    setTimeout(() => {
      this.logOut();
    }, 1000)
  }

  logOut() {
    localStorage.clear();
    this._sharedService.uu = null;
    console.log("logged out", window.location);
    if(window.location.pathname != "/auth/logout" && window.location.pathname != "/auth/login"){
      localStorage.setItem("lasturl", window.location.pathname);
    }
    
    this._router.navigate(["/auth/login"]);
  }

  isLoggedIn() {
    console.log("isLoggedIn", localStorage)
    if(localStorage.getItem("uu") != null){
      this._sharedService.uu = JSON.parse(localStorage.getItem("uu"));
      return true;
    }
    return false;
  }

  goTo(app: string){
    this._router.navigate([app]);
  }
}
