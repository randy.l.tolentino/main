import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Location } from '@angular/common';
import { SharedService } from 'projects/rtlib/src/public-api';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(
    private _authService: AuthService,
    private _router: Router,
    private _sharedService: SharedService,
    private _location: Location){
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(this._authService.isLoggedIn() && this._sharedService.uu){
         console.log(this._sharedService.uu);
         if(this._sharedService.uu.isAdmin){
           return true;
         }
      }
      this._router.navigate(["403"], {skipLocationChange: true, replaceUrl: false});
      this._location.replaceState(state.url);
      return false;
  }
  
}
