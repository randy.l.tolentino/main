import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { LoginComponent } from './auth/login/login.component';
import { UnauthorizedComponent } from './auth/unauthorized/unauthorized.component';


const routes: Routes = [
  {path: '', redirectTo: 'shop', pathMatch: 'full'},
  {path: '403', component: UnauthorizedComponent, canActivate: [AuthGuard]},
  {path: 'auth/:action', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes/* , {useHash: true} */)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
