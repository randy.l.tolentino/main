import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopAppComponent } from './app.component';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { RiderComponent } from './rider/rider.component';
import { SellerComponent } from './seller/seller.component';
import { AdminComponent } from './admin/admin.component';
import { AdminGuard } from 'src/app/auth/admin.guard';

const routes: Routes = [
  {path: "admin", component: AdminComponent, canActivate: [AdminGuard]},
  {path: "rider/:store", component: RiderComponent, canActivate: [AuthGuard]},
  {path: "rider", component: RiderComponent, canActivate: [AuthGuard]},
  {path: "seller/:store", component: SellerComponent, canActivate: [AuthGuard]},
  {path: "seller", component: SellerComponent, canActivate: [AuthGuard]},
  {path: "shop/:store", component: ShopAppComponent, canActivate: [AuthGuard]},
  {path: "shop", component: ShopAppComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
