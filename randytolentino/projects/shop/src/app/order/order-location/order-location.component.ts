import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-order-location',
  templateUrl: './order-location.component.html',
  styles: [`
    max-height: unset;
    height: 87%;
    overflow-y: auto;`
  ]})
export class OrderLocationComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data){}

  myPosition;
  isTracking: boolean;

  addressList: {name: string}[];

  ngOnInit(){
    if (navigator.geolocation) {
      this.isTracking = true;
      if(this.data.action=="trackAsRider"){
        navigator.geolocation.watchPosition((position) => {
          /* this.showTrackingPosition(position); */
          console.log('position', position);
          this.myPosition = position;
        });
      } else if(this.data.action == "set"){

      }
      
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  newAddress(data: any){
    console.log('recevied new address', data);
    this.addressList = [];
    this.addressList = data.results.map(addr => ({name: addr.address_components[0].short_name}));
  }

}
