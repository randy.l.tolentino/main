import { AfterViewInit, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SharedService } from 'projects/rtlib/src/public-api';
import { OrderLocationComponent } from './order/order-location/order-location.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class ShopAppComponent implements OnInit, AfterViewInit{
  title = 'USO Shopping';
  constructor(private _md: MatDialog,
    private _sharedService: SharedService
    /* ,
    pluck: PluckPipe */){}
    
  public user;
  public isViewInit: boolean;

  public attrsOpt = ['size', 'pieces', 'color']

  //store: currency, productType attributes[size, pieces, color]
  public products = [
  {
    id: 1,
    name: "Classic Milk Tea",
    categoryId: 1,
    defaultPrice: null,
    defaultAttr: "",
    defaultImage: "1",
    isNew: false,
    selections: [],
    productTypes: [
      {
        attr: {
          size: {value: 'L', label: 'Large'},
          pieces: null, color: null
        },
        price: 73,
        image: null,
        description: "A taste of Large Milk Tea",
        isDefault: true
      },
      {
        attr: {
          size: {value: 'M', label: 'Medium'},
          pieces: null, color: null
        },
        price: 63,
        image: null,
        description: "A taste of Medium Milk Tea"
      }
    ]
  },
  {
    id: 2,
    name: "Oreo Madness",
    categoryId: 1,
    defaultPrice: null,
    defaultAttr: "",
    defaultImage: "2",
    defaultDescription: "",
    isNew: false,
    selections: [],
    productTypes: [
      {
        attr: {
          size: {value: 'L', label: 'Large'},
          pieces: null, color: null
        },
        price: 78,
        image: null,
        description: "A taste of Large Oreo Madness",
        isDefault: true
      },
      {
        attr: {
          size: {value: 'M', label: 'Medium'},
          pieces: null, color: null
        },
        price: 68,
        image: null,
        description: "A taste of Medium Oreo Madness"
      }
    ]
  },
  {
    id: 3,
    name: "Red Velvet",
    categoryId: 1,
    defaultPrice: null,
    defaultAttr: "",
    defaultImage: "3",
    defaultDescription: "",
    isNew: true,
    selections: [],
    productTypes: [
      {
        attr: {
          size: {value: 'L', label: 'Large'},
          pieces: null, color: null
        },
        price: 78,
        image: null,
        description: "A taste of Large Red Velvet",
        isDefault: true
      },
      {
        attr: {
          size: {value: 'M', label: 'Medium'},
          pieces: null, color: null
        },
        price: 68,
        image: null,
        description: "A taste of Medium Red Velvet"
      }
    ]
  },
  {
    id: 4,
    name: "Cheesecake",
    categoryId: 1,
    defaultPrice: null,
    defaultAttr: "",
    defaultImage: "4b",
    defaultDescription: "",
    isNew: true,
    selections: [],
    productTypes: [
      {
        attr: {
          size: {value: 'L', label: 'Large'},
          pieces: null, color: null
        },
        price: 78,
        image: null,
        description: "A taste of Large Cheesecake",
        isDefault: true
      },
      {
        attr: {
          size: {value: 'M', label: 'Medium'},
          pieces: null, color: null
        },
        price: 68,
        image: null,
        description: "A taste of Medium Cheesecake"
      }
    ]
  },
  {
    id: 5,
    name: "Wintermelon",
    categoryId: 1,
    defaultPrice: null,
    defaultAttr: "",
    defaultImage: "5b",
    defaultDescription: "",
    isNew: false,
    selections: [],
    productTypes: [
      {
        attr: {
          size: {value: 'L', label: 'Large'},
          pieces: null, color: null
        },
        price: 78,
        image: null,
        description: "A taste of Large Wintermelon",
        isDefault: true
      },
      {
        attr: {
          size: {value: 'M', label: 'Medium'},
          pieces: null, color: null
        },
        price: 68,
        image: null,
        description: "A taste of Medium Wintermelon"
      }
    ]
  },
  {
    id: 6,
    name: "French Fries",
    categoryId: 2,
    defaultPrice: null,
    defaultAttr: "",
    defaultImage: "6",
    defaultDescription: "",
    isNew: false,
    selections: [],
    productTypes: [
      {
        attr: {
          size: {value: 'L', label: 'Large'},
          pieces: null, color: null
        },
        price: 68,
        image: null,
        description: "A taste of Large French Fries",
        isDefault: true
      },
      {
        attr: {
          size: {value: 'M', label: 'Medium'},
          pieces: null, color: null
        },
        price: 38,
        image: null,
        description: "A taste of Medium French Fries"
      }
    ]
  },
  {
    id: 7,
    name: "Chicken Wings",
    categoryId: 2,
    defaultPrice: null,
    defaultAttr: "",
    defaultImage: "5",
    defaultDescription: "",
    isNew: false,
    selections: [],
    productTypes: [
      {
        attr: {
          size: null,
          pieces: {value: '4', label: "4 Pieces"}, color: null
        },
        price: 118,
        image: null,
        description: "A taste of 6 Pieces Chicken Wings",
        isDefault: true
      },
      {
        attr: {
          size: null,
          pieces: {value: '6', label: "6 Pieces"}, color: null
        },
        price: 148,
        image: null,
        description: "A taste of 6 Pieces Chicken Wings"
      },
      {
        attr: {
          size: null,
          pieces: {value: '8', label: "8 Pieces"}, color: null
        },
        price: 198,
        image: null,
        description: "A taste of 8 Pieces Chicken Wings"
      }
    ]
  },
  {
    id: 8,
    name: "T-Shirt",
    categoryId: 2,
    defaultPrice: null,
    defaultAttr: "",
    defaultImage: "5",
    defaultDescription: "",
    isNew: false,
    selections: [],
    productTypes: [
      {
        attr: {
          size: {value: 'S', label: 'Small'},
          pieces: null, color: {value: 'W', label: 'White'}
        },
        price: 99,
        image: null,
        description: "",
        isDefault: true
      },
      {
        attr: {
          size:  {value: 'M', label: 'Medium'},
          pieces: null, color:  {value: 'W', label: 'White'}
        },
        price: 109,
        image: null,
        description: ""
      },
      {
        attr: {
          size: {value: 'L', label: 'Large'},
          pieces: null, color: {value: 'W', label: 'White'}
        },
        price: 119,
        image: null,
        description: ""
      },
      {
        attr: {
          size: {value: 'S', label: 'Small'},
          pieces: null, color: {value: 'B', label: 'Black'}
        },
        price: 108,
        image: null,
        description: "",
        isDefault: true
      },
      {
        attr: {
          size:  {value: 'M', label: 'Medium'},
          pieces: null, color:  {value: 'B', label: 'Black'}
        },
        price: 118,
        image: null,
        description: ""
      },
      {
        attr: {
          size: {value: 'L', label: 'Large'},
          pieces: null, color: {value: 'B', label: 'Black'}
        },
        price: 128,
        image: null,
        description: ""
      }
    ]
  }].map(p => {
    const defP = (p.productTypes as any).find(ppT => ppT.isDefault);
    if(defP){
      p.defaultPrice = defP.price;
      p.defaultAttr = Object.keys(defP.attr).map(a => (defP.attr[a] ? defP.attr[a].label : null)).filter(b => b!=null).join(" - ");
      if(defP.image){
        p.defaultImage = defP.image;
      }
      p.defaultDescription = defP.description;
    }
    return p;
  });

  getSelections(item): {price: number, selections: string} {
    let price = item.defaultPrice;
    const foundItem = (item.productTypes || []).find(ipt => {
      let isFound = true;
        item.selections.forEach(its => {
          if(ipt.attr[its.key] && ipt.attr[its.key].label != its.value){
            isFound = false;
          }
        });
      return isFound;
    });
    if(foundItem){
      price = foundItem.price;
    }

    return {price, selections: item.selections.map(s => s.value).join(" - ")}
  }

  public enabledAttrs(ptAttr): string[]{
    return this.attrsOpt.filter(attr => ptAttr[attr]);
  }

  public composeLabel(): string{
    return null;
  }

  public optionCLick(item, selectedPrice){
    item.defaultPrice = selectedPrice;
    //item.selectedLabel = selectedLabel;
  }

  orderAndSetLocation(action: string){
    console.log("user", this.user);
    const data ={
      user: this.user,
      action
    }
    console.log("user", data);
    
    this._md.open
      (OrderLocationComponent,
      {
        width: '99vw', height: '98vh', 
        data,
        disableClose: true
      });
  }

  ngOnInit(){
    console.log("shop init", this._sharedService);
    this.user = this._sharedService.uu;
  }

  getItemSelections(item){
    return item.selections;
  }
  
  ngAfterViewInit(){
    this.isViewInit = true;
  }

  addToCart(pT: any){
    /* console.log("addToCart", pT); */

    const cartItem = Object.assign({}, {name: pT.name, selections: Object.assign([], pT.selections)});

    /* this._sharedService.basket = [
      ...this._sharedService.basket,
      ...[{...{}, ...cartItem}]
    ]; */

    this._sharedService.basket.push(cartItem);
    
    //.push({...{}, ...cartItem});

    /* console.log("cart", this._sharedService.basket); */
  }
}
