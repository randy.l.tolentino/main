import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ShopRoutingModule } from './app-routing.module';
import { ShopAppComponent } from './app.component';
import { ShopService } from './services/shop.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment as env } from 'src/environments/environment';
import { ShopServiceImpl } from './services/impl/shop-impl.service';
import { ShopServiceImplMock } from './services/impl/mock/shop-impl-mock.service';
import { OrderLocationComponent } from './order/order-location/order-location.component';
import { RtlibModule } from 'projects/rtlib/src/public-api';
import { AdminComponent } from './admin/admin.component';
import { RiderComponent } from './rider/rider.component';
import { SellerComponent } from './seller/seller.component';

export function shopServiceInstance(http: HttpClient){
  if(env.production && !env.maintenance){
    return new ShopServiceImpl(http);
  } else{
    return new ShopServiceImplMock(http);
  }
}

@NgModule({
  declarations: [
    ShopAppComponent,
    OrderLocationComponent,
    AdminComponent,
    RiderComponent,
    SellerComponent
  ],
  imports: [
    BrowserModule,
    ShopRoutingModule,
    RtlibModule
  ],
  providers: [
    {provide: ShopService, useFactory: shopServiceInstance, deps: [HttpClient]}
  ],
  bootstrap: [ShopAppComponent]
})
export class ShopAppModule { }
