import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ShopService } from '../../shop.service';
import { environment as env } from 'src/environments/environment';
import { Observable, of } from 'rxjs';

@Injectable()
export class ShopServiceImplMock extends ShopService{
  constructor(private _http: HttpClient) {
    super();
  }

  private _apiUrl = env.apiUrl;

  login(params: any): Observable<any> {
    return of({"responseMessage":null,"responseCode":"SUCCESS","data":{"userData":{"id":1,"username":null,"email":"randy.l.tolentino@gmail.com","password":null,"isAdmin":true,"isOwner":null,"isRider":null,"stores":[],"lastShop":null},"socialData":{"id":"10158732108639594","name":"Randy L. Tolentino","email":"randy.l.tolentino@gmail.com"}}});
  }

  getLogin(params: any): Observable<any> {
    return this._http.get(`${this._apiUrl}/login/${params.token}`);
  }
}
