import { TestBed } from '@angular/core/testing';

import { ShopServiceImplMock } from './shop-impl-mock.service';

describe('ShopImplMockService', () => {
  let service: ShopServiceImplMock;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShopServiceImplMock);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
