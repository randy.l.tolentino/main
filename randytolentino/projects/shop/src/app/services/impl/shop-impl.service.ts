import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ShopService } from '../shop.service';
import { environment as env } from 'src/environments/environment'

@Injectable()
export class ShopServiceImpl extends ShopService{
  
  constructor(private _http: HttpClient) {
    super();
  }

  private _apiUrl = env.apiUrl;

  login(params: any): Observable<any> {
    return this._http.post(`${this._apiUrl}/login`, params);
  }

  getLogin(params: any): Observable<any> {
    return this._http.get(`${this._apiUrl}/login/${params.token}`);
  }
}
