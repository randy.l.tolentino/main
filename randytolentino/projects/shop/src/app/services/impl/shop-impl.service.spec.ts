import { TestBed } from '@angular/core/testing';

import { ShopServiceImpl } from './shop-impl.service';

describe('ShopImplService', () => {
  let service: ShopServiceImpl;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShopServiceImpl);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
