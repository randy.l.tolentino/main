import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export abstract class ShopService {

  constructor() { }

  abstract login(params: any): Observable<any>;
  abstract getLogin(params: any): Observable<any>;
}
