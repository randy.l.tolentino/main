import { NgModule } from '@angular/core';
import { RtlibComponent } from './rtlib.component';

import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MapsComponent } from './maps/maps.component';
import { GoogleMapsModule } from '@angular/google-maps'
import { CommonModule } from '@angular/common';
import { GridModule } from './grid/grid.module';
import { ChipsComponent } from './chips/chips.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatDividerModule } from '@angular/material/divider';
import { MatBadgeModule } from '@angular/material/badge';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { PluckPipe } from './pipes.pipe';
import { FlexLayoutModule } from '@angular/flex-layout';

const ieModules: any[] = [
  GridModule,
  MatDialogModule,
  MatButtonModule,
  GoogleMapsModule,
  MatChipsModule,
  MatFormFieldModule,
  MatIconModule,
  MatMenuModule,
  MatDividerModule,
  MatBadgeModule,
  MatGridListModule,
  MatCardModule,
  MatButtonToggleModule,
  MatSidenavModule,
  FlexLayoutModule
];

@NgModule({
  declarations: [RtlibComponent, MapsComponent, ChipsComponent, PluckPipe],
  imports: [
    CommonModule,
    ].concat(ieModules),
  exports: [
    RtlibComponent, 
    MapsComponent,
    ChipsComponent,
    PluckPipe,
  ].concat(ieModules),
})
export class RtlibModule { }
