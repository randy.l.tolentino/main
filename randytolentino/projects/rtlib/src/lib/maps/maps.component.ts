import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'rt-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss'],
})
export class MapsComponent implements OnInit {
  myPosMarker: google.maps.Marker;

  constructor(private _http: HttpClient) { }

  // Accessing HTML Element 
  @ViewChild('mapContainer', { static: false }) gmap: ElementRef; 

  @Input() loc: any;
  @Input() action: string;
  productLat;                                                          
  productLong;                                                          
  userLat;                                                               
  userLong;
  userCoordinates;
  map: google.maps.Map;
  listingPlace;                                                    
  distanceProductToUser;
  marker;

  @Output() dragMyPlace = new EventEmitter<any>();

  ngOnInit(){
    /* // showing map                                     
    this.mapInitializer();
    // get our location                                     
    this.getLocation();                                                   
    //calculate our location distance to product                  
    this.calculateDistanceUserToProduct();     */                                      
  }
  // ANGULAR ON AFTER VIEW LIFECYCLE WILL BE CALLED AFTER OUR LAYOUT DONE RENDERING
  ngAfterViewInit() {          
    console.log("action", this.action);                                         
    /* this.mapInitializer(); */
    this.getLocation();
    
    if(this.action == "track"){
      this.calculateDistanceUserToProduct();
    } else{

    }                             
  }

  getLocation() {
    navigator.geolocation.getCurrentPosition((position) => {            
        this.userLat = position.coords.latitude;                                
        this.userLong = position.coords.longitude;
        
        //added, marker for current location
        this.userCoordinates = new google.maps.LatLng(this.userLat, this.userLong); 
        // init our coordinate for marker :)
        this.myPosMarker = new google.maps.Marker({                                       
          position: this.userCoordinates,                                                         
          map: this.map,
          label: "My Location",
          draggable: true,
          crossOnDrag: true,                                    
        });

        this.myPosMarker.addListener("dragend", (a) => {
          console.log("a lat", a);
          this.userLat = a.latLng.lat();                                
          this.userLong = a.latLng.lng();
          /* this.listingPlace; */

          this.initPosition(true);
        });

        this.mapInitializer();
      }, error => {                                                          
        alert("Please Allow Google Maps in your Browser");                                
      })

  }

  calculateDistanceUserToProduct() {
    const earthRadius = 6371 // Radius of the earth in km
    const dLat = this.deg2rad(this.productLat - this.userLat); 
    const dLon = this.deg2rad(this.productLong - this.userLong);
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(this.deg2rad(this.userLat)) * Math.cos(this.deg2rad(this.productLat)) *                               
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));                        
    const dist = earthRadius * c; // Distance in km                         
    const finalDist = dist.toFixed(2);                           
    if(finalDist == 'NaN'){                                
      this.distanceProductToUser = null;                                        
    } else {                                                    
      this.distanceProductToUser = finalDist;                                          
    }                                                                   
    console.log("final dist", this.distanceProductToUser);                           
}

  deg2rad(deg) {                                                            
    return deg * (Math.PI / 180);
  }

  mapInitializer() {
    // check here to get to know about map options docs                        
    const mapOptions: google.maps.MapOptions = { 
      // here to place centering our location based on coordinates                                                                                        
      zoom: 15,                                                         
      fullscreenControl: true,                                    
      mapTypeControl: false,                                         
      streetViewControl: false,                                     
    };

    // place your product lat or longtitude here
    if(this.action == "trackByCustomer" || this.action == "set"){
      this.productLat = this.loc.lat; // change it to your preferences                                           
      this.productLong = this.loc.long; // <-- static                                                                  
      const coordinates = new google.maps.LatLng(this.productLat, this.productLong); 
      // init our coordinate for marker :)
      this.marker = new google.maps.Marker({                                       
        position: coordinates,                                                         
        map: this.map,
        label: "My Milk Tea",                                                    
      });

      mapOptions.center = this.userCoordinates;

      console.log("this.userCoordinates", )

      // get address name based on latitude and longtitude 
      // here we do Reverse Geocoding technique that convert Latitude and 
      // longtitude to be lovely human readable information like address 
      this.initPosition();
    }
 
    //set our lovely google maps here and marker here
    this.map = new google.maps.Map(this.gmap.nativeElement, mapOptions);
    this.marker.setMap(this.map);
  }

  initPosition(dragged?: boolean){
    this.getCenterMap().subscribe((data: any) => { 
      if(!dragged){
        this.myPosMarker.setMap(this.map);
      } 
      this.dragMyPlace.emit(data);
      console.log("product address map data", data);
      this.listingPlace = 
      // doing traverse from json response that we get from //getProductAddressMap() below
        data.results[1].address_components[0].short_name;
      this.calculateDistanceUserToProduct();                 
    });
  }

  getCenterMap() {
    let addressName;
    if(this.action == "set"){
      addressName = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.userLat},${this.userLong}`;
    } else{
      addressName = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.productLat},${this.productLong}`;
    }

    addressName += '&key=AIzaSyB6iWBsrkJ2daqi65mIUiBMeQdxXwT8k4g';
    return this._http.get(addressName);
  }
      
    
}
