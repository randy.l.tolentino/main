import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { GridApi, ServerSideStoreType } from 'ag-grid-community';
import { ColumnApi } from 'ag-grid-community/dist/lib/columnController/columnApi';
import { ApiCall } from '../shared/classes/interfaces';
import { GridType } from './classes/enums';
import { ExtendedGridDefinition } from './classes/interfaces';


function createServerSideDatasource(server?) {
  return {
    getRows: function (params) {
      console.log(
        '[Datasource] - rows requested by grid: startRow = ' +
          params.request.startRow +
          ', endRow = ' +
          params.request.endRow
      );
      var response = server ? server.getData(params.request): {};
      setTimeout(function () {
        if (response.success) {
          params.success({
            rowData: response.rows,
            rowCount: response.lastRow,
          });
        } else {
          params.fail();
        }
      });
    },
  };
}
function createFakeServer(allData) {
  return {
    getData: function (request) {
      //console.log("request", request);
      var requestedRows = allData.slice(request.startRow, request.endRow);
      //console.log("requestedRows", requestedRows);
      var lastRow = getLastRowIndex(request, requestedRows);
      //console.log("lastRow", lastRow);
      return {
        success: true,
        rows: requestedRows,
        lastRow: lastRow,
      };
    },
  };
}
function getLastRowIndex(request, results) {
  if (!results) return undefined;
  var currentLastRow = request.startRow + results.length;
  return currentLastRow < request.endRow ? currentLastRow : undefined;
}

@Component({
  selector: 'rt-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit, AfterViewInit {

  constructor() { 
    if(!this.columnDefs){
      //get it
    }

    this.defaultColDef = {...this.defaultColDef, ...this._getDefaultColdef()};
  }

  @Input() gridId;
  @Input() theme = "ag-theme-alpine";
  @Input() style = "width: 100vw; height: 100vh";
  @Input() class;
  @Input() columnDefs;
  @Input() defaultColDef: any
  @Input() rowModelType: GridType =  GridType.ClientSide;
  @Input() serverSideStoreType: ServerSideStoreType =  ServerSideStoreType.Full;
  @Input() rowData: any[] = [];
  @Input() sideBar: string[] = ['filters', 'columns'];
  @Input() apiCall: ApiCall;
  @Input() eg: ExtendedGridDefinition = {};

  @ViewChild('agGrid') public agGrid: AgGridAngular;
  gridApi: GridApi;
  gridColumnApi: ColumnApi;
  
  ngOnInit(): void {}

  onGridReady(params): void {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    if(this.apiCall){
      this.apiCall.api(this.apiCall.params).subscribe(data => {
        if(this.rowModelType == "clientSide"){
          params.api.setRowData(data);
        } else{
          var fakeServer = createFakeServer(data);
          var datasource = createServerSideDatasource(fakeServer);
          params.api.setServerSideDatasource(datasource);
        }
      }, err => {
        if(this.apiCall.failCallback){
          this.apiCall.failCallback({err, p: params});
        }
      }, () => {
        if(this.apiCall.successCallback){
          this.apiCall.successCallback(params);
        }
      });
    }
  }

  private _getDefaultColdef(): any {
    return {
      flex: 1,
      minWidth: 100,
      sortable: true,
      enableValue: true,
      enableRowGroup: true,
      enablePivot: true,
    };
  }

  ngAfterViewInit(): void {}
}
