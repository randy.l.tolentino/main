export enum GridType {
    ServerSide = "serverSide",
    ClientSide = "clientSide",
    ViewPort = "viewPort"
}