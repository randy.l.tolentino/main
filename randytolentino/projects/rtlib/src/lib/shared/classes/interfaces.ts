import { Observable } from "rxjs";

export interface ApiCall {
    api: (params?) => Observable<any>;
    params?: any;
    successCallback?: (p?: any) => void;
    failCallback?: (p?: any) => void;
}
