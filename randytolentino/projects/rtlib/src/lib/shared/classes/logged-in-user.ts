export interface LoggedInUser {
    id: number;
    name: string;
    email: string;
    photoUrl: string;
    firstName: string;
    lastName: string;
    authToken: string;
    response: Response;
    provider: string;
    username?: any;
    password?: any;
    isAdmin: boolean;
    isOwner?: any;
    isRider?: any;
    stores: any[];
    lastShop?: any;
}

interface Response {
    name: string;
    email: string;
    picture: Picture;
    first_name: string;
    last_name: string;
    id: string;
}

interface Picture {
    data: Data;
}

interface Data {
    height: number;
    is_silhouette: boolean;
    url: string;
    width: number;
}