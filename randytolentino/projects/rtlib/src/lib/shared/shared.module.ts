import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedService } from './shared.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class SharedModule { 
  constructor( @Optional() @SkipSelf() parentModule?: SharedModule){
    if(parentModule){
      throw new Error('SharedModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(config: { sharedService: SharedService}): ModuleWithProviders<SharedModule>{
    return {
      ngModule: SharedModule,
      providers: [{provide: SharedService, useValue: config.sharedService}]
    }
  }
}
