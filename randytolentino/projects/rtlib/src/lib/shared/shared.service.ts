import { Injectable, Optional } from '@angular/core';
import { Subject } from 'rxjs';
import { LoggedInUser } from './classes/logged-in-user';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  constructor(@Optional() config?: SharedService) { 
    if(config){
      /* this.loggedInUser = config.loggedInUser; */
      this.uu = config.uu;
      this.loggingIn = config.loggingIn;
      this.basket = config.basket;
    }
  }

  /* public loggedInUser: Subject<any>; */
  public uu: LoggedInUser;
  public loggingIn: Subject<boolean>;
  public basket: any[];
}
