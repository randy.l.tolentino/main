import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { ApiCall } from 'dist/rtlib/lib/shared/classes/interfaces';
import { GridComponent } from 'rtlib';
import { StaticsService } from './statics.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit{
  title = 'eclipsetea';
  public user: SocialUser;
  columnDefs: any[]
  apiCall: ApiCall;

  @ViewChild("grid") grid: GridComponent;

  constructor(private http: HttpClient,
    public md:MatDialog,
    private _authService: SocialAuthService){

    this.columnDefs = [
      {
        field: 'athlete',
        minWidth: 220,
        filter: 'agTextColumnFilter',
      },
      {
        field: 'country',
        minWidth: 200,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: [
            'United States',
            'Ireland',
            'United Kingdom',
            'Russia',
            'Australia',
            'Canada',
            'Brazil',
            'Norway',
          ],
        },
      },
      {
        field: 'year',
        filter: 'agNumberColumnFilter',
      },
      {
        field: 'sport',
        minWidth: 200,
      },
      { field: 'gold', filter: 'agNumberColumnFilter'},
      { field: 'silver', filter: 'agNumberColumnFilter'},
      { field: 'bronze', filter: 'agNumberColumnFilter'},
    ];
    this.apiCall = {api: StaticsService.getMock};
  }

  ngAfterViewInit(): void {
    this.grid.agGrid.gridReady.subscribe(p => {
      console.log("ag grid gridReady event", p);
    });
  }

  clicked(action: string){
    this.md.open
      (ImNewComponent, 
      {
        width: '99vw', height: '98vh', 
        data: {
          user: this.user,
          action
        },
        disableClose: true
      });
  }

  ngOnInit(){
    this._authService.authState.subscribe((user) => {
      this.user = user;
    });
  }
}

@Component({
  selector: 'dialog-yo',
  template: `
  <button mat-button mat-dialog-close style="float: right"><mat-icon>close</mat-icon></button>
  <span mat-dialog-title style="font-size: 12px" [ngSwitch]="data.action">
    Hi {{data.user.name}}, <span *ngSwitchCase="'set'">please verify your location.</span>
    <span *ngSwitchDefault>your Milk Tea is on its way!</span>
  </span>
  <div mat-dialog-content style="max-height: 80% !important;">
    <rt-maps [action]="data.action" [loc]="{lat: 15.7175613, long: 120.9093055}"
      (dragMyPlace)="newAddress($event)">
    </rt-maps>
    <rt-chips [placeholder]="'Replace or enter another address not listed below'" [options]="addressList"></rt-chips>
  </div>
  <div style="display: flex">
    <div mat-dialog-actions align="start" style="width:25%">
      <span style="font-size: 9px"><i>Maximize the map when setting your location for better view.</i></span>
    </div>
    <div mat-dialog-actions align="end" style="width:75%">
      <button mat-raised-button color="warn" mat-dialog-close>OK, all set!</button>
      <!-- <button mat-raised-button color="primary">Ok</button> -->
    </div>
  </div>
  
  `,
  styles: [`
    max-height: unset;
    height: 87%;
    overflow-y: auto;`
  ]
})
export class ImNewComponent implements OnInit{

  constructor(@Inject(MAT_DIALOG_DATA) public data){}
  myPosition;
  isTracking: boolean;

  addressList: {name: string}[];

  ngOnInit(){
    if (navigator.geolocation) {
      this.isTracking = true;
      if(this.data.action=="trackAsRider"){
        navigator.geolocation.watchPosition((position) => {
          /* this.showTrackingPosition(position); */
          console.log('position', position);
          this.myPosition = position;
        });
      } else if(this.data.action == "set"){

      }
      
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  newAddress(data: any){
    console.log('recevied new address', data);
    this.addressList = [];
    this.addressList = data.results.map(addr => ({name: addr.address_components[0].short_name}));
  }
}