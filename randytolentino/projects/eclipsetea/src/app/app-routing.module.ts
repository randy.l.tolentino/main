import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from 'src/app/auth/auth.guard';


const routes: Routes = [
  {path: "eclipse/:store", component: AppComponent, canActivate: [AuthGuard]},
  {path: "eclipse", component: AppComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
