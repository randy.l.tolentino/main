import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent, ImNewComponent } from './app.component';
import { RtlibModule } from 'projects/rtlib/src/public-api';

@NgModule({
  declarations: [
    AppComponent, ImNewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RtlibModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class EclipseModule { }
