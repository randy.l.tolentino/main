package com.randytolentino.uso.repositories;

import java.util.List;

import com.randytolentino.uso.classes.model.Store;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepo extends JpaRepository<Store, Long>{
    Store findByName(String name);
    List<Store> findByOwnerId(Long ownerId);
}
