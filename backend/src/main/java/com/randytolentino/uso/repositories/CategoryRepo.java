package com.randytolentino.uso.repositories;

import com.randytolentino.uso.classes.model.Category;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepo extends JpaRepository<Category, Long>{
    Category findByName(String name);
}
