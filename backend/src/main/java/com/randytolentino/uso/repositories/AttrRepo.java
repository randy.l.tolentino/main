package com.randytolentino.uso.repositories;

import com.randytolentino.uso.classes.model.Attr;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AttrRepo extends JpaRepository<Attr, Long>{
    Attr findByAttrKey(String attrKey);
}
