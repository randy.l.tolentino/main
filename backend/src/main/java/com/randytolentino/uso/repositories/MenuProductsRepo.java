package com.randytolentino.uso.repositories;

import com.randytolentino.uso.classes.model.MenuProducts;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuProductsRepo extends JpaRepository<MenuProducts, Long>{
    
}
