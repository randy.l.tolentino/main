package com.randytolentino.uso.repositories;

import com.randytolentino.uso.classes.model.Users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsersRepo extends JpaRepository<Users, Long>{
    @Query("Select u from Users u left join fetch u.stores WHERE u.email = :email")
    Users findByEmail(String email);
}
