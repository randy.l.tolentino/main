package com.randytolentino.uso.repositories;

import com.randytolentino.uso.classes.model.AttrKey;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AttrKeyRepo extends JpaRepository<AttrKey, Long> {
    AttrKey findByAttrKey(String attrKey);
}
