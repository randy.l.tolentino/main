package com.randytolentino.uso.repositories;

import com.randytolentino.uso.classes.model.ProductType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductTypeRepo extends JpaRepository<ProductType, Long>{
    
}
