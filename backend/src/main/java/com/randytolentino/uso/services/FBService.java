package com.randytolentino.uso.services;

import com.randytolentino.uso.classes.model.reponse.SocialData;

public interface FBService {
    SocialData validateToken(String token) throws Exception;
}
