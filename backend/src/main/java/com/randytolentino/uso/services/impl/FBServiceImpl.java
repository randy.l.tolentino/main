package com.randytolentino.uso.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.randytolentino.uso.classes.model.reponse.SocialData;
import com.randytolentino.uso.services.FBService;
import com.randytolentino.uso.utils.JsonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FBServiceImpl implements FBService{
    @Autowired
    private RestTemplate restTemplate;

    public SocialData validateToken(String token) throws Exception {
        String uri = "https://graph.facebook.com/me?fields=id,name,email&access_token=" + token + "";
        return restTemplate.getForObject(uri, SocialData.class);
    }
}
