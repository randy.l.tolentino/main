package com.randytolentino.uso.utils;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/* @Log4j2 */
@Service public class ApiClient {

    /* @Autowired private RestTemplate restTemplate;

    public <T> T callRequest(HttpMethod method, Map<String, String> mapHeaders, String uri, Object obj,
                             Class<T> clazz) throws HttpServerErrorException, HttpClientErrorException {

        final HttpEntity request = new HttpEntity( obj //,createHeaders( mapHeaders ) );
        log.info("request: " + JsonUtil.objectToJson(request));
        return (T) restTemplate.exchange(uri, method, request, clazz);

    } */

    /* private HttpHeaders createHeaders( Map< String, String > mapHeaders ) {
        HttpHeaders headers = new HttpHeaders( );
        headers.set( "client_id", PayoutConstants.CLIENT_ID );
        headers.set( "client_secret", PayoutConstants.CLIENT_SECRET );
        headers.setContentType( MediaType.APPLICATION_JSON_UTF8 );
        mapHeaders.forEach( ( headerName, headerValue ) -> headers.add( headerName, headerValue ) );
        return headers;
    } */
}
