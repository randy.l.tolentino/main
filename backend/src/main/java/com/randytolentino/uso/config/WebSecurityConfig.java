package com.randytolentino.uso.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.autoconfigure.session.SessionRepositoryUnavailableException;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

/* import org.springframework.web.cors.CorsConfigurationSource;
 */
import com.randytolentino.uso.classes.CsrfHeaderFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private Environment env;

    private boolean disableAppLogin = false;
    private int serverPort = 8080;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        if(env.getProperty("disable-app.login")!=null && !env.getProperty("disable-app.login").isEmpty()){
            disableAppLogin =  Boolean.valueOf(env.getProperty("disable-app.login"));
        }

        
        if(!disableAppLogin) {
            //for login and logout (without cas)
            http
                .authorizeRequests()
                .antMatchers("/resources/**", "/registration", "/autoconfig","/listener/passwordHint/**","/img/**", "/webjars/**","/gs-guide-websocket/**","/gs-guide-websocket").permitAll()
                .antMatchers("/users/**", "/roles/**","/recruitments/**","/candidates/**","/mod_history/**",
                        "/configuration/jobLevel/all",
                        "/configuration/candidateRole/all",
                        "/configuration/skills/all",
                        "/configuration/hiringManager/all",
                        "/configuration/source/all",
                        "/configuration/capability/all",
                        "/configuration/project/all",
                        "/configuration/statusReason/all"
                    ).hasAnyRole("USER", "MANAGER")
                .antMatchers("/reports/**").hasAnyRole("REPORT", "MANAGER")
                .antMatchers("/import/**").hasAnyRole("UPLOAD", "MANAGER")
                .antMatchers("/configuration/**").hasRole("MANAGER")

                .anyRequest().authenticated()
            .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
            .and()
                .logout()
                .permitAll()
            .and().cors();
        }
        else{
            http
                .authorizeRequests()
                .anyRequest().permitAll()
            .and().cors()
            .and().csrf().disable()
            //.ignoringAntMatchers(CSRF_IGNORE) // URI where CSRF check will not be applied
            /* .and().csrf()
                .csrfTokenRepository(csrfTokenRepository()) // defines a repository where tokens are stored
				.and()
				.addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class) */;
        }

        /* http.cors(); */

        //for csrf
        /* http
            .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
            .csrf().csrfTokenRepository(csrfTokenRepository()); */
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    private CsrfTokenRepository csrfTokenRepository(){
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName("XSRF-TOKEN");
        return repository;
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() 
    {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setAllowedOrigins(Arrays.asList("https://localhost:4200", "https://rltolentino.com", "http://127.0.0.1:443"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT", "HEAD", "OPTIONS", "PATCH"));
        
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}