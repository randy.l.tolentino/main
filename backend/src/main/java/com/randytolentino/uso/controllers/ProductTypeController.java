package com.randytolentino.uso.controllers;

import java.util.List;

import com.randytolentino.uso.classes.EndpointResult;
import com.randytolentino.uso.classes.RESPONSE_CODE;
import com.randytolentino.uso.classes.model.ProductType;
import com.randytolentino.uso.repositories.ProductTypeRepo;
import com.randytolentino.uso.utils.JsonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RequestMapping(value = "api")
@RestController
@Log4j2
public class ProductTypeController {
    @Autowired
    ProductTypeRepo productTypeRepo;

    @GetMapping(value = "productType")
    public EndpointResult<List<ProductType>> getProductType(){
        log.info("Controller_getProductType (List): " + this.getClass());
        EndpointResult<List<ProductType>> eProductType = new EndpointResult<>();

        try {
            List<ProductType> lu = productTypeRepo.findAll();
            eProductType.addData(lu);
        } catch (Exception e) {
            eProductType.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getProductTypeResult: " + JsonUtil.objectToJson(eProductType));
        return eProductType;
    }

    @GetMapping(value = "productType/{id}")
    public EndpointResult<ProductType> getProductType(@PathVariable Long id){
        log.info("Controller_getProductType: " + this.getClass());
        EndpointResult<ProductType> eProductType = new EndpointResult<>();

        try {
            ProductType u = productTypeRepo.getOne(id);
            eProductType.addData(u);
        } catch (Exception e) {
            eProductType.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getProductTypeResult: " + JsonUtil.objectToJson(eProductType));
        return eProductType;
    }

    @PostMapping(value = "productType")
    public EndpointResult<ProductType> createProductType(@RequestBody ProductType productType){
        log.info("Controller_createProductType: " + this.getClass());
        EndpointResult<ProductType> eProductType = new EndpointResult<>();

        try {
            ProductType u = productTypeRepo.saveAndFlush(productType);
            eProductType.addData(u);
        } catch (Exception e) {
            eProductType.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("createProductTypeResult: " + JsonUtil.objectToJson(eProductType));
        return eProductType;
    }

    @PutMapping(value = "productType")
    public EndpointResult<ProductType> updateProductType(@RequestBody ProductType productType){
        log.info("Controller_updateProductType: " + this.getClass());
        EndpointResult<ProductType> eProductType = new EndpointResult<>();

        try {
            ProductType u = productTypeRepo.save(productType);
            eProductType.addData(u);
        } catch (Exception e) {
            eProductType.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("updateProductTypeResult: " + JsonUtil.objectToJson(eProductType));
        return eProductType;
    }
    
    @DeleteMapping(value = "productType/{id}")
    public EndpointResult<Boolean> delete(@PathVariable Long id){
        log.info("Controller_deleteProductType: " + this.getClass());
        EndpointResult<Boolean> deleteResult = new EndpointResult<>();

        try {
            ProductType u = productTypeRepo.getOne(id);
            productTypeRepo.delete(u);
            deleteResult.addData(true);
        } catch (Exception e) {
            deleteResult.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("deleteProductTypeResult: " + JsonUtil.objectToJson(deleteResult));
        return deleteResult;
    }
}
