package com.randytolentino.uso.controllers;

import java.util.List;

import com.randytolentino.uso.classes.EndpointResult;
import com.randytolentino.uso.classes.RESPONSE_CODE;
import com.randytolentino.uso.classes.model.AttrKey;
import com.randytolentino.uso.repositories.AttrKeyRepo;
import com.randytolentino.uso.utils.JsonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RequestMapping(value = "api")
@RestController
@Log4j2
public class AttrKeyController {
    @Autowired
    AttrKeyRepo attrKeyRepo;

    @GetMapping(value = "attrKey")
    public EndpointResult<List<AttrKey>> getAttrKey(){
        log.info("Controller_getAttrKey (List): " + this.getClass());
        EndpointResult<List<AttrKey>> eAttrKey = new EndpointResult<>();

        try {
            List<AttrKey> lu = attrKeyRepo.findAll();
            eAttrKey.addData(lu);
        } catch (Exception e) {
            eAttrKey.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getAttrKeyResult: " + JsonUtil.objectToJson(eAttrKey));
        return eAttrKey;
    }

    @GetMapping(value = "attrKey/{id}")
    public EndpointResult<AttrKey> getAttrKey(@PathVariable Long id){
        log.info("Controller_getAttrKey: " + this.getClass());
        EndpointResult<AttrKey> eAttrKey = new EndpointResult<>();

        try {
            AttrKey u = attrKeyRepo.getOne(id);
            eAttrKey.addData(u);
        } catch (Exception e) {
            eAttrKey.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getAttrKeyResult: " + JsonUtil.objectToJson(eAttrKey));
        return eAttrKey;
    }

    @PostMapping(value = "attrKey")
    public EndpointResult<AttrKey> createAttrKey(@RequestBody AttrKey attrKey){
        log.info("Controller_createAttrKey: " + this.getClass());
        EndpointResult<AttrKey> eAttrKey = new EndpointResult<>();

        try {
            AttrKey u = attrKeyRepo.saveAndFlush(attrKey);
            eAttrKey.addData(u);
        } catch (Exception e) {
            eAttrKey.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("createAttrKeyResult: " + JsonUtil.objectToJson(eAttrKey));
        return eAttrKey;
    }

    @PutMapping(value = "attrKey")
    public EndpointResult<AttrKey> updateAttrKey(@RequestBody AttrKey attrKey){
        log.info("Controller_updateAttrKey: " + this.getClass());
        EndpointResult<AttrKey> eAttrKey = new EndpointResult<>();

        try {
            AttrKey u = attrKeyRepo.save(attrKey);
            eAttrKey.addData(u);
        } catch (Exception e) {
            eAttrKey.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("updateAttrKeyResult: " + JsonUtil.objectToJson(eAttrKey));
        return eAttrKey;
    }
    
    @DeleteMapping(value = "attrKey/{id}")
    public EndpointResult<Boolean> delete(@PathVariable Long id){
        log.info("Controller_deleteAttrKey: " + this.getClass());
        EndpointResult<Boolean> deleteResult = new EndpointResult<>();

        try {
            AttrKey u = attrKeyRepo.getOne(id);
            attrKeyRepo.delete(u);
            deleteResult.addData(true);
        } catch (Exception e) {
            deleteResult.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("deleteAttrKeyResult: " + JsonUtil.objectToJson(deleteResult));
        return deleteResult;
    }
}
