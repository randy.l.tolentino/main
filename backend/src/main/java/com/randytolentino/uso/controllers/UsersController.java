package com.randytolentino.uso.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import com.randytolentino.uso.classes.EndpointResult;
import com.randytolentino.uso.classes.RESPONSE_CODE;
import com.randytolentino.uso.classes.model.Store;
import com.randytolentino.uso.classes.model.Users;
import com.randytolentino.uso.repositories.UsersRepo;
import com.randytolentino.uso.utils.JsonUtil;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RequestMapping(value = "api")
@RestController
@Log4j2
public class UsersController {
    @Autowired
    UsersRepo usersRepo;

    @Autowired
    EntityManager entityManager;

    @GetMapping(value = "users")
    public EndpointResult<List<Users>> getUsers(){
        log.info("Controller_getUsers (List): " + this.getClass());
        EndpointResult<List<Users>> eUsers = new EndpointResult<>();

        try {
            List<Users> lu = usersRepo.findAll();
            eUsers.addData(lu);
        } catch (Exception e) {
            eUsers.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getUsersResult: " + JsonUtil.objectToJson(eUsers));
        return eUsers;
    }

    @GetMapping(value = "users/{email}")
    public EndpointResult<Users> getUsers(@PathVariable String email){
        log.info("Controller_getUsers: " + this.getClass());
        EndpointResult<Users> eUsers = new EndpointResult<>();
        try {
            Users u = usersRepo.findByEmail(email);
            log.info("usersRepo result: " + JsonUtil.objectToJson(u));
            eUsers.addData(u);
        } catch (Exception e) {
            eUsers.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getUsersResult: " + JsonUtil.objectToJson(eUsers));
        return eUsers;
    }

    @PostMapping(value = "users")
    public EndpointResult<Users> createUsers(@RequestBody Users users){
        log.info("Controller_createUsers: " + this.getClass());
        EndpointResult<Users> eUsers = new EndpointResult<>();
        try {
            Users u = usersRepo.saveAndFlush(users);
            eUsers.addData(u);
        } catch (Exception e) {
            eUsers.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("createUsersResult: " + JsonUtil.objectToJson(eUsers));
        return eUsers;
    }

    @PutMapping(value = "users")
    public EndpointResult<Users> updateUsers(@RequestBody Users users){
        log.info("Controller_updateUsers: " + this.getClass());
        EndpointResult<Users> eUsers = new EndpointResult<>();

        try {
            Users u = usersRepo.save(users);
            eUsers.addData(u);
        } catch (Exception e) {
            eUsers.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("updateUsersResult: " + JsonUtil.objectToJson(eUsers));
        return eUsers;
    }
    
    @DeleteMapping(value = "users/{id}")
    public EndpointResult<Boolean> delete(@PathVariable Long id){
        log.info("Controller_deleteUsers: " + this.getClass());
        EndpointResult<Boolean> deleteResult = new EndpointResult<>();

        try {
            Users u = usersRepo.getOne(id);
            usersRepo.delete(u);
            deleteResult.addData(true);
        } catch (Exception e) {
            deleteResult.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("deleteUsersResult: " + JsonUtil.objectToJson(deleteResult));
        return deleteResult;
    }
}
