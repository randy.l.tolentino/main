package com.randytolentino.uso.controllers;

import java.util.List;

import com.randytolentino.uso.classes.EndpointResult;
import com.randytolentino.uso.classes.RESPONSE_CODE;
import com.randytolentino.uso.classes.model.Store;
import com.randytolentino.uso.repositories.StoreRepo;
import com.randytolentino.uso.utils.JsonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RequestMapping(value = "api")
@RestController
@Log4j2
public class StoreController {
    @Autowired
    StoreRepo storeRepo;

    @GetMapping(value = "store")
    public EndpointResult<List<Store>> getStore(){
        log.info("Controller_getStore (List): " + this.getClass());
        EndpointResult<List<Store>> eStore = new EndpointResult<>();

        try {
            List<Store> lu = storeRepo.findAll();
            eStore.addData(lu);
        } catch (Exception e) {
            eStore.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getStoreResult: " + JsonUtil.objectToJson(eStore));
        return eStore;
    }

    @GetMapping(value = "store/{ownerId}")
    public EndpointResult<List<Store>> getStoreByOwner(long ownerId){
        log.info("Controller_getStoreByOwner (List): " + this.getClass());
        EndpointResult<List<Store>> eStore = new EndpointResult<>();

        try {
            List<Store> lu = storeRepo.findByOwnerId(Long.valueOf(ownerId));
            eStore.addData(lu);
        } catch (Exception e) {
            eStore.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getStoreResult: " + JsonUtil.objectToJson(eStore));
        return eStore;
    }

    @GetMapping(value = "store/{name}")
    public EndpointResult<Store> getStore(@PathVariable String name){
        log.info("Controller_getStore: " + this.getClass());
        EndpointResult<Store> eStore = new EndpointResult<>();

        try {
            Store u = storeRepo.findByName(name);
            eStore.addData(u);
        } catch (Exception e) {
            eStore.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getStoreResult: " + JsonUtil.objectToJson(eStore));
        return eStore;
    }

    @PostMapping(value = "store")
    public EndpointResult<Store> createStore(@RequestBody Store store){
        log.info("Controller_createStore: " + this.getClass());
        EndpointResult<Store> eStore = new EndpointResult<>();

        try {
            Store u = storeRepo.saveAndFlush(store);
            eStore.addData(u);
        } catch (Exception e) {
            eStore.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("createStoreResult: " + JsonUtil.objectToJson(eStore));
        return eStore;
    }

    @PutMapping(value = "store")
    public EndpointResult<Store> updateStore(@RequestBody Store store){
        log.info("Controller_updateStore: " + this.getClass());
        EndpointResult<Store> eStore = new EndpointResult<>();

        try {
            Store u = storeRepo.save(store);
            eStore.addData(u);
        } catch (Exception e) {
            eStore.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("updateStoreResult: " + JsonUtil.objectToJson(eStore));
        return eStore;
    }
    
    @DeleteMapping(value = "store/{id}")
    public EndpointResult<Boolean> delete(@PathVariable Long id){
        log.info("Controller_deleteStore: " + this.getClass());
        EndpointResult<Boolean> deleteResult = new EndpointResult<>();

        try {
            Store u = storeRepo.getOne(id);
            storeRepo.delete(u);
            deleteResult.addData(true);
        } catch (Exception e) {
            deleteResult.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("deleteStoreResult: " + JsonUtil.objectToJson(deleteResult));
        return deleteResult;
    }
}
