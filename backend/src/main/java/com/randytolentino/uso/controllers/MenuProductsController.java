package com.randytolentino.uso.controllers;

import java.util.List;

import com.randytolentino.uso.classes.EndpointResult;
import com.randytolentino.uso.classes.RESPONSE_CODE;
import com.randytolentino.uso.classes.model.MenuProducts;
import com.randytolentino.uso.repositories.MenuProductsRepo;
import com.randytolentino.uso.utils.JsonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RequestMapping(value = "api")
@RestController
@Log4j2
public class MenuProductsController {
    @Autowired
    MenuProductsRepo menuProductsRepo;

    @GetMapping(value = "menuProducts")
    public EndpointResult<List<MenuProducts>> getMenuProducts(){
        log.info("Controller_getMenuProducts (List): " + this.getClass());
        EndpointResult<List<MenuProducts>> eMenuProducts = new EndpointResult<>();

        try {
            List<MenuProducts> lu = menuProductsRepo.findAll();
            eMenuProducts.addData(lu);
        } catch (Exception e) {
            eMenuProducts.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getMenuProductsResult: " + JsonUtil.objectToJson(eMenuProducts));
        return eMenuProducts;
    }

    @GetMapping(value = "menuProducts/{id}")
    public EndpointResult<MenuProducts> getMenuProducts(@PathVariable Long id){
        log.info("Controller_getMenuProducts: " + this.getClass());
        EndpointResult<MenuProducts> eMenuProducts = new EndpointResult<>();

        try {
            MenuProducts u = menuProductsRepo.getOne(id);
            eMenuProducts.addData(u);
        } catch (Exception e) {
            eMenuProducts.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getMenuProductsResult: " + JsonUtil.objectToJson(eMenuProducts));
        return eMenuProducts;
    }

    @PostMapping(value = "menuProducts")
    public EndpointResult<MenuProducts> createMenuProducts(@RequestBody MenuProducts menuProducts){
        log.info("Controller_createMenuProducts: " + this.getClass());
        EndpointResult<MenuProducts> eMenuProducts = new EndpointResult<>();

        try {
            MenuProducts u = menuProductsRepo.saveAndFlush(menuProducts);
            eMenuProducts.addData(u);
        } catch (Exception e) {
            eMenuProducts.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("createMenuProductsResult: " + JsonUtil.objectToJson(eMenuProducts));
        return eMenuProducts;
    }

    @PutMapping(value = "menuProducts")
    public EndpointResult<MenuProducts> updateMenuProducts(@RequestBody MenuProducts menuProducts){
        log.info("Controller_updateMenuProducts: " + this.getClass());
        EndpointResult<MenuProducts> eMenuProducts = new EndpointResult<>();

        try {
            MenuProducts u = menuProductsRepo.save(menuProducts);
            eMenuProducts.addData(u);
        } catch (Exception e) {
            eMenuProducts.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("updateMenuProductsResult: " + JsonUtil.objectToJson(eMenuProducts));
        return eMenuProducts;
    }
    
    @DeleteMapping(value = "menuProducts/{id}")
    public EndpointResult<Boolean> delete(@PathVariable Long id){
        log.info("Controller_deleteMenuProducts: " + this.getClass());
        EndpointResult<Boolean> deleteResult = new EndpointResult<>();

        try {
            MenuProducts u = menuProductsRepo.getOne(id);
            menuProductsRepo.delete(u);
            deleteResult.addData(true);
        } catch (Exception e) {
            deleteResult.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("deleteMenuProductsResult: " + JsonUtil.objectToJson(deleteResult));
        return deleteResult;
    }
}
