package com.randytolentino.uso.controllers;

import java.util.ArrayList;
import java.util.List;

import com.randytolentino.uso.classes.EndpointResult;
import com.randytolentino.uso.classes.RESPONSE_CODE;
import com.randytolentino.uso.classes.model.Users;
import com.randytolentino.uso.classes.model.reponse.AuthResponse;
import com.randytolentino.uso.classes.model.reponse.HelloModel;
import com.randytolentino.uso.classes.model.reponse.SocialData;
import com.randytolentino.uso.classes.model.request.SocialToken;
import com.randytolentino.uso.repositories.UsersRepo;
import com.randytolentino.uso.services.FBService;
import com.randytolentino.uso.utils.JsonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import lombok.extern.log4j.Log4j2;

@RequestMapping(value = "api")
@RestController
/* @CrossOrigin("https://rltolentino.com") */
@CrossOrigin("https://localhost:4200")
@Log4j2
public class AuthController {
    @Autowired
    private FBService fbService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired 
    private UsersRepo usersRepo;

    @GetMapping(value = "/getSomething")
    public List<HelloModel> getSomething() {
        List<HelloModel> aL = new ArrayList<HelloModel>();
        HelloModel a = new HelloModel();
        a.setHName("randy");
        aL.add(a);

        HelloModel b = new HelloModel();
        b.setHName("tolentino");
        aL.add(b);

        return aL;
    }

    @GetMapping(value = "/login/{token}")
    public Object login(@PathVariable String token) {
        System.out.println(JsonUtil.objectToJson(token));
        String uri = "https://graph.facebook.com/me?fields=id,name,email&access_token=" + token + "";
        Object response = restTemplate.getForObject(uri, Object.class);

        log.info(JsonUtil.objectToJson(response));
        return response;
    }

    @PostMapping(value = "/login")
    public EndpointResult<AuthResponse> login(@RequestBody SocialToken token) {
        log.info("Controller_login: " + this.getClass());

        System.out.println(JsonUtil.objectToJson(token));

        EndpointResult<AuthResponse> loginResult = new EndpointResult<>();
        AuthResponse authResponse = new AuthResponse();

        try {
            SocialData validatedTokenWithResult = this.fbService.validateToken(token.getToken());
            if(validatedTokenWithResult != null){
                authResponse.setSocialData(validatedTokenWithResult);

                Users u = usersRepo.findByEmail(validatedTokenWithResult.getEmail());
                if(u != null && u.getEmail() != null){
                    authResponse.setUserData(u);
                } else{
                    u = new Users();
                    u.setEmail(validatedTokenWithResult.getEmail());
                    authResponse.setUserData(usersRepo.saveAndFlush(u));
                }

                loginResult.addData(authResponse);
            } else{
                loginResult.addError(RESPONSE_CODE.FB_CALL_ERROR, "No ValidationToken Result");
                log.error("No ValidationToken Result");
            }
            
        } catch (Exception e) {
            loginResult.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("loginResult: " + JsonUtil.objectToJson(loginResult));
        return loginResult;
    }
}