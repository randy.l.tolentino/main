package com.randytolentino.uso.controllers;

import java.util.List;

import com.randytolentino.uso.classes.EndpointResult;
import com.randytolentino.uso.classes.RESPONSE_CODE;
import com.randytolentino.uso.classes.model.Category;
import com.randytolentino.uso.repositories.CategoryRepo;
import com.randytolentino.uso.utils.JsonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RequestMapping(value = "api")
@RestController
@Log4j2
public class CategoryController {
    @Autowired
    CategoryRepo categoryRepo;

    @GetMapping(value = "category")
    public EndpointResult<List<Category>> getCategory(){
        log.info("Controller_getCategory (List): " + this.getClass());
        EndpointResult<List<Category>> eCategory = new EndpointResult<>();

        try {
            List<Category> lu = categoryRepo.findAll();
            eCategory.addData(lu);
        } catch (Exception e) {
            eCategory.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getCategoryResult: " + JsonUtil.objectToJson(eCategory));
        return eCategory;
    }

    @GetMapping(value = "category/{id}")
    public EndpointResult<Category> getCategory(@PathVariable Long id){
        log.info("Controller_getCategory: " + this.getClass());
        EndpointResult<Category> eCategory = new EndpointResult<>();

        try {
            Category u = categoryRepo.getOne(id);
            eCategory.addData(u);
        } catch (Exception e) {
            eCategory.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getCategoryResult: " + JsonUtil.objectToJson(eCategory));
        return eCategory;
    }

    @PostMapping(value = "category")
    public EndpointResult<Category> createCategory(@RequestBody Category category){
        log.info("Controller_createCategory: " + this.getClass());
        EndpointResult<Category> eCategory = new EndpointResult<>();

        try {
            Category u = categoryRepo.saveAndFlush(category);
            eCategory.addData(u);
        } catch (Exception e) {
            eCategory.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("createCategoryResult: " + JsonUtil.objectToJson(eCategory));
        return eCategory;
    }

    @PutMapping(value = "category")
    public EndpointResult<Category> updateCategory(@RequestBody Category category){
        log.info("Controller_updateCategory: " + this.getClass());
        EndpointResult<Category> eCategory = new EndpointResult<>();

        try {
            Category u = categoryRepo.save(category);
            eCategory.addData(u);
        } catch (Exception e) {
            eCategory.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("updateCategoryResult: " + JsonUtil.objectToJson(eCategory));
        return eCategory;
    }
    
    @DeleteMapping(value = "category/{id}")
    public EndpointResult<Boolean> delete(@PathVariable Long id){
        log.info("Controller_deleteCategory: " + this.getClass());
        EndpointResult<Boolean> deleteResult = new EndpointResult<>();

        try {
            Category u = categoryRepo.getOne(id);
            categoryRepo.delete(u);
            deleteResult.addData(true);
        } catch (Exception e) {
            deleteResult.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("deleteCategoryResult: " + JsonUtil.objectToJson(deleteResult));
        return deleteResult;
    }
}
