package com.randytolentino.uso.controllers;

import java.util.List;

import com.randytolentino.uso.classes.EndpointResult;
import com.randytolentino.uso.classes.RESPONSE_CODE;
import com.randytolentino.uso.classes.model.Attr;
import com.randytolentino.uso.repositories.AttrRepo;
import com.randytolentino.uso.utils.JsonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RequestMapping(value = "api")
@RestController
@Log4j2
public class AttrController {
    @Autowired
    AttrRepo attrRepo;

    @GetMapping(value = "attr")
    public EndpointResult<List<Attr>> getAttr(){
        log.info("Controller_getAttr (List): " + this.getClass());
        EndpointResult<List<Attr>> eAttr = new EndpointResult<>();

        try {
            List<Attr> lu = attrRepo.findAll();
            eAttr.addData(lu);
        } catch (Exception e) {
            eAttr.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getAttrResult: " + JsonUtil.objectToJson(eAttr));
        return eAttr;
    }

    @GetMapping(value = "attr/{id}")
    public EndpointResult<Attr> getAttr(@PathVariable Long id){
        log.info("Controller_getAttr: " + this.getClass());
        EndpointResult<Attr> eAttr = new EndpointResult<>();

        try {
            Attr u = attrRepo.getOne(id);
            eAttr.addData(u);
        } catch (Exception e) {
            eAttr.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("getAttrResult: " + JsonUtil.objectToJson(eAttr));
        return eAttr;
    }

    @PostMapping(value = "attr")
    public EndpointResult<Attr> createAttr(@RequestBody Attr attr){
        log.info("Controller_createAttr: " + this.getClass());
        EndpointResult<Attr> eAttr = new EndpointResult<>();

        try {
            Attr u = attrRepo.saveAndFlush(attr);
            eAttr.addData(u);
        } catch (Exception e) {
            eAttr.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("createAttrResult: " + JsonUtil.objectToJson(eAttr));
        return eAttr;
    }

    @PutMapping(value = "attr")
    public EndpointResult<Attr> updateAttr(@RequestBody Attr attr){
        log.info("Controller_updateAttr: " + this.getClass());
        EndpointResult<Attr> eAttr = new EndpointResult<>();

        try {
            Attr u = attrRepo.save(attr);
            eAttr.addData(u);
        } catch (Exception e) {
            eAttr.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("updateAttrResult: " + JsonUtil.objectToJson(eAttr));
        return eAttr;
    }
    
    @DeleteMapping(value = "attr/{id}")
    public EndpointResult<Boolean> delete(@PathVariable Long id){
        log.info("Controller_deleteAttr: " + this.getClass());
        EndpointResult<Boolean> deleteResult = new EndpointResult<>();

        try {
            Attr u = attrRepo.getOne(id);
            attrRepo.delete(u);
            deleteResult.addData(true);
        } catch (Exception e) {
            deleteResult.addError(RESPONSE_CODE.DB_CALL_ERROR, e.getMessage() + " " + e.getCause());
            log.error(e.getMessage() + " " + e.getCause());
        }        

        log.info("deleteAttrResult: " + JsonUtil.objectToJson(deleteResult));
        return deleteResult;
    }
}
