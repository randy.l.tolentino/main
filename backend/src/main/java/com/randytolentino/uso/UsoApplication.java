package com.randytolentino.uso;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class UsoApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsoApplication.class, args);
	}

	@Value( "${app.timezone}")
	private String timezone;

	@PostConstruct
	void started(){
		TimeZone.setDefault(TimeZone.getTimeZone(timezone));
	}

	/* @SpringBootApplication
	@ComponentScan
	@EnableAutoConfiguration
	public class Application extends SpringBootServletInitializer{
		@Override
		protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
			return application.sources(UsoApplication.class);
		}
	} */

	@Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
	}
}
