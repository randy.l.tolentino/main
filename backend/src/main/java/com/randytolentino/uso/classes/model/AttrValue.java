package com.randytolentino.uso.classes.model;

import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class AttrValue {
    private String label;
    private char[] value;
}
