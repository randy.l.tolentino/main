package com.randytolentino.uso.classes.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.randytolentino.uso.classes.model.common.BaseEntity;
import lombok.Data;

@Data
@Entity
@Table(name = "users")
public class Users extends BaseEntity<Long> implements Serializable {
    private String username;
    private String email;
    private String password;
    private Boolean isAdmin;
    private Boolean isOwner;
    private Boolean isRider;

    @OneToMany(mappedBy = "owner" /* ,cascade = CascadeType.ALL,orphanRemoval = true */)
    /* @Cache(usage = CacheConcurrencyStrategy.READ_WRITE) */
    private List<Store> stores;

    @OneToOne
    @JoinColumn(name = "last_shop_id", nullable = true)
    private Store lastShop;

    @JsonIgnore
    public void merge(Users other) {
        /* this.a = other.a == null ? this.a : other.a;
        this.b.addAll(other.b);
        this.c = other.c == 0 ? this.c : other.c; */
    }
}
