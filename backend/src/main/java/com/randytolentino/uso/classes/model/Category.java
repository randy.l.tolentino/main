package com.randytolentino.uso.classes.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.randytolentino.uso.classes.model.common.BaseEntity;

import lombok.Data;

@Data
@Entity
public class Category extends BaseEntity<Long> implements Serializable {
    private String name;

    @OneToMany(mappedBy = "category")
    private List<MenuProducts> products;

}
