package com.randytolentino.uso.classes;

public enum RESPONSE_CODE {
    SUCCESS(0, "Successful Request"),
    HTTP_CALL_ERROR(1, "Http Call Error"),
    DB_CALL_ERROR(2, "Database Call Error"),
    FB_CALL_ERROR(1, "FB Call Error");

    public String message;
    public int code;

    RESPONSE_CODE(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static RESPONSE_CODE parse(int code) {
        RESPONSE_CODE status = null; // Default
        for (RESPONSE_CODE item : RESPONSE_CODE.values()) {
            if (item.code == code) {
                status = item;
                break;
            }
        }
        return status;
    }
}
