package com.randytolentino.uso.classes.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.randytolentino.uso.classes.model.common.BaseEntity;

import lombok.Data;

@Data
@Entity
@Table(name = "product_type")
public class ProductType extends BaseEntity<Long> implements Serializable {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id", nullable = false)
    private MenuProducts product;
    
    /* @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "prodtype_attrs", 
      joinColumns = @JoinColumn(name = "attr_id", referencedColumnName = "id", nullable = false), 
      inverseJoinColumns = @JoinColumn(name = "product_type_id", referencedColumnName = "id"))
     */
    @OneToMany(mappedBy = "productType")
    private List<Attr> attrs;

    private Double price;
    private String image;
    private String description;
    private Boolean isDefault;
}
