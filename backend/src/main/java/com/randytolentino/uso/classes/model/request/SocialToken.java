package com.randytolentino.uso.classes.model.request;

import lombok.Data;

@Data
public class SocialToken {
    private String token;
}
