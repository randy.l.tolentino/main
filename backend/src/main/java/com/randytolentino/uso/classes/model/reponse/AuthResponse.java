package com.randytolentino.uso.classes.model.reponse;

import com.randytolentino.uso.classes.model.Users;

import lombok.Data;

@Data
public class AuthResponse {
    private Users userData;
    private SocialData socialData;
}
