package com.randytolentino.uso.classes.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.randytolentino.uso.classes.model.common.BaseEntity;

import lombok.Data;

@Data
@Entity
@Table(name = "attr_key")
public class AttrKey extends BaseEntity<Long> implements Serializable {
    String attrKey;

    @OneToOne(mappedBy = "attrKey")
    @JsonIgnore
    Attr attr;

    @ManyToMany(cascade={CascadeType.ALL}, fetch = FetchType.EAGER,
    mappedBy = "attrKeys")
    @JsonIgnore
    List<Store> stores;
}
