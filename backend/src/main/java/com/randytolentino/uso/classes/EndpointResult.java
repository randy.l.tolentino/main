package com.randytolentino.uso.classes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* import com.randytolentino.uso.utils.ErrorCodeUtil; */

import lombok.Getter;
import lombok.Setter;
/* import lombok.extern.log4j.Log4j2; */

/* @Log4j2*/
public class EndpointResult<T> {

	/* private ErrorCodeUtil error = new ErrorCodeUtil(); */
	@Getter
	private String responseMessage;

	@Getter
	private RESPONSE_CODE responseCode = RESPONSE_CODE.SUCCESS;

	@Getter
	@Setter
	private T data;

	public EndpointResult() {
		setOk();

	}

	public EndpointResult(EndpointResult<T> result) {
		addResult(result);
	}

	/**
	 * Merge the fields of an Endpoint result to this. New input takes precedence
	 * over existing.
	 * 
	 * @param result
	 */
	public void addResult(EndpointResult<T> result) {
		this.responseCode = result.responseCode;
		this.responseMessage = result.responseMessage;
		this.data = result.data;
	}

	/**
	 * Add just data to this result.
	 * 
	 * @return
	 */
	public void addData(T data) {
		this.data = data;
	}

	public void setOk() {
		responseCode = RESPONSE_CODE.SUCCESS;
		setResponseMessage(responseCode);
	}

	public void addError(RESPONSE_CODE responseCode) {
		this.responseCode = responseCode;
		setResponseMessage(responseCode);
	}

	public void addError(RESPONSE_CODE responseCode, String responseMessage) {
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}

	public void setResponseMessage(RESPONSE_CODE responseMessage) {
		/* this.responseMessage = error.getErrorMessage(responseCode); */
	}

	public void addError(RESPONSE_CODE responseCode, List params) {
		this.responseCode = responseCode;
		setResponseMessage(responseCode.toString(), params);
	}

	public void setResponseMessage(String responseMessage, List params) {
		/* this.responseMessage = error.getErrorMessage(responseCode); */
		for (int i = 0; i < params.size(); i++) {
			this.responseMessage = this.responseMessage.replace("{" + i + "}", params.get(i).toString());
		}
	}

	public Object getData(String key) {
		// TODO: get a member of an object
		return null;
	}

	public boolean hasErrors() {
		return (!responseCode.equals(RESPONSE_CODE.SUCCESS));
	}

	/**
	 * Set your own unique data key for the result set. Used if you don't want to
	 * use the default "data" key being returned.
	 * 
	 * @param key
	 * @param value
	 */
	public void addData(String key, Object value) {
		if (value != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(key, value);
			this.data = (T) map;
		}
	}

}
