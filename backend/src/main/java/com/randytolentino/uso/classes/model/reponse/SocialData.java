package com.randytolentino.uso.classes.model.reponse;

import lombok.Data;

@Data
public class SocialData {
    public SocialData()
    {
     super();
    }

    public SocialData(String id, String name, String email){
        this.id = id;
        this.name = name;
        this.email = email;
    }

    private String id;
    private String name;
    private String email;
}
