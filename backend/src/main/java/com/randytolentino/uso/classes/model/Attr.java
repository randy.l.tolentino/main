package com.randytolentino.uso.classes.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.randytolentino.uso.classes.model.common.BaseEntity;

import org.springframework.data.rest.core.annotation.RestResource;

import lombok.Data;

@Data
@Entity
@Table(name = "attr")
public class Attr extends BaseEntity<Long> implements Serializable {
    /* @ManyToMany(mappedBy = "attrs")
    @JsonIgnore */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id", nullable = false)
    ProductType productType;
    
    @Embedded
    private AttrValue attrValue;

    @OneToOne
    @JoinColumn(name = "attr_key_id")
    @RestResource(path = "attrAttrKey", rel="attrKey")
    private AttrKey attrKey;
}
