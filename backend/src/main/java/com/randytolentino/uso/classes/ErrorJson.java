package com.randytolentino.uso.classes;

import java.util.Map;

/**
 * Created by rltolentino on 9/19/2016.
 */

public class ErrorJson {

public Integer status;
public String error;
public String message;
public String timeStamp;
public String trace;

public ErrorJson(int status, Map<String, Object> errorAttributes) {
        this.status = status;
        this.error = (String) errorAttributes.get("error");
        this.message = (String) errorAttributes.get("message");
        if(errorAttributes.get("timestamp") != null) {
                this.timeStamp = errorAttributes.get("timestamp") .toString();
        }
        else if(errorAttributes.get("timeStamp") != null) {
                this.timeStamp = errorAttributes.get("timeStamp") .toString();
        }
        else{
                this.timeStamp = null;
        }
        this.trace = (String) errorAttributes.get("trace");
    }
}