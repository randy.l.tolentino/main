package com.randytolentino.uso.classes.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.randytolentino.uso.classes.model.common.BaseEntity;

import org.springframework.lang.Nullable;

import lombok.Data;

/* @EqualsAndHashCode(callSuper = true) */
/* @EntityListeners */

@Data
@Entity
public class Store extends BaseEntity<Long> implements Serializable {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", nullable = false)
    private Users owner;

    @OneToMany(mappedBy = "store")
    private List<MenuProducts> products = new ArrayList<>();

    private String name;
    private String description;
    private String currency;

    private String greeting;
    private String logo;
    private String coverPhoto;
    
    @Embedded
    private Address address;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "stores_attrkey", 
      joinColumns = @JoinColumn(name = "attr_key_id", referencedColumnName = "id", nullable = true), 
      inverseJoinColumns = @JoinColumn(name = "store_id", referencedColumnName = "id"))
    @Nullable
    private List<AttrKey> attrKeys  = new ArrayList<>();

    public Optional getAttrKeys(){
      return Optional.ofNullable(attrKeys);
    }
}