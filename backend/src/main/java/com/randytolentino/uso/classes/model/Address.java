package com.randytolentino.uso.classes.model;

import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class Address {
    Integer locLong;
    Integer locLat;
    String line1;
    String line2;
    Integer postalCode;
    String city;
    String province;
    String countryCode;
}
