package com.randytolentino.uso.classes.model;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.randytolentino.uso.classes.model.common.BaseEntity;

import org.springframework.lang.Nullable;

import lombok.Data;

@Data
@Entity
@Table(name = "products")
public class MenuProducts extends BaseEntity<Long> implements Serializable {
    private String name;
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    private Double price;
    private String image;
    private Boolean isNew;

    @OneToMany(cascade={CascadeType.ALL}, fetch = FetchType.EAGER,
        mappedBy = "product")
    @Nullable
    private List<ProductType> productTypes;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "store_id", nullable = false)
    private Store store;
}