/*CREATE PROCEDURE lisaa_vakio_rivit(IN idTarjous INT, IN idMalli INT, IN inSopKau INT, IN inKm INT, OUT status_out BOOLEAN)
 MODIFIES SQL DATA
 BEGIN ATOMIC
  DECLARE done INT DEFAULT FALSE;
  DECLARE a, e, f, intKPL, g, intVertaa INT (10);
  DECLARE b, c VARCHAR (500);
  DECLARE d, desSumma DECIMAL (10,2);
  DECLARE cur1 CURSOR FOR SELECT mr1.ID014, mr1.Tyyppi, mr1.Selite, mr1.Hinta, mr1.Eka_Huolto_Km, mr1.Normi_Huolto_Vali, mr1.Vahintaan_kpl
  FROM mallikohtaiset_rivit AS mr1
  WHERE mr1.MALLI_ID002=idMalli AND mr1.Vakio=1
  ORDER BY MALLI_ID002, Lajittelu;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN cur1;
  SET status_out = FALSE;

  read_loop: LOOP
    FETCH cur1 INTO a, b, c, d, e, f, g;

    IF done THEN
      LEAVE read_loop;
    END IF;

    	SET intVertaa = g * inSopKau;
		SET intKPL = FLOOR((inKm * inSopKau - e) / f +1);

        IF intKPL < intVertaa THEN
        	SET intKPL = intVertaa;
        	SET desSumma = intKPL * d;
            INSERT INTO mallikohtaiset (tarjous_id009, Tyyppi, Selite, Hinta, kpl, Summa, mallikohtaiset_rivit_id014)
		   	SELECT idTarjous, b, c, d, intKPL, desSumma, a;
        ELSEIF intKPL >= intVertaa THEN
    	   SET desSumma = intKPL * d;
            INSERT INTO mallikohtaiset (tarjous_id009, Tyyppi, Selite, Hinta, kpl, Summa, mallikohtaiset_rivit_id014)
		    SELECT idTarjous, b, c, d, intKPL, desSumma, a;
	   END IF;
  END LOOP;
  CLOSE cur1;

	INSERT INTO mallikohtaiset (tarjous_id009, Tyyppi, Selite, Hinta, kpl, Summa, mallikohtaiset_rivit_id014)
	SELECT idTarjous, mr.Tyyppi, mr.Selite, mr.Hinta, mr.kpl, mr.Summa, mr.ID014
	FROM mallikohtaiset_rivit AS mr
		WHERE mr.MALLI_ID002=idMalli AND  mr.Vakio=2
		ORDER BY MALLI_ID002, Lajittelu;

    INSERT INTO mallikohtaiset (tarjous_id009, Tyyppi, Selite, Hinta, kpl, Summa, mallikohtaiset_rivit_id014)
	SELECT idTarjous, mr.Tyyppi, mr.Selite, mr.Hinta, mr.kpl, mr.Summa, mr.ID014
	FROM mallikohtaiset_rivit AS mr
		WHERE mr.Vakio=3
		ORDER BY Lajittelu;
	SET status_out = TRUE;
END ^;*/

/*CREATE PROCEDURE `laskee_kk_kustannukset`(IN `idTarjous` INT, IN `inSopKau` INT, IN `inLasKau` INT, IN `inSopAlkaa` DATE, IN `inAlennus` DECIMAL(2,2))
    COMMENT 'Poistaa tarjouksen kuukausi kustannukset ja lisää ne uudelleen'
BEGIN

/* JH 24.8.2017 
/* remake RT 24.5.2019 

DECLARE desKkEra DECIMAL (10,2);
DECLARE count INT DEFAULT 1;
DECLARE i INT DEFAULT 0;
DECLARE x INT DEFAULT 0;
DECLARE kk INT DEFAULT 0;

DECLARE desSum1 DECIMAL (10,2);
DECLARE desSum2 DECIMAL (10,2);
DECLARE desSopEur DECIMAL (10,2);

SET status_out = FALSE;
DELETE FROM kk_erat WHERE tarjous_ID009 = idTarjous;

SELECT mk.tarjous_ID009, Sum(mk.Summa) AS Summa INTO x, desSum1 FROM mallikohtaiset AS mk WHERE mk.tarjous_ID009 = idTarjous AND rivit_type=14;
SELECT mm.tarjous_ID009, Sum(mm.Summa) AS Summa INTO x, desSum2 FROM mallikohtaiset AS mm WHERE mm.tarjous_ID009 = idTarjous AND rivit_type=17;

if desSum1 != 0 then
	set desSum1 = desSum1;
		else
			set desSUm1 = 0;
end if;

if desSum2 != 0 then
	set desSum2 = desSum2;
		else
			set desSUm2 = 0;
end if;

SET desSopEur = desSum1 + desSum2;
SET desSopEur =  desSopEur * (1-inAlennus) * inLasKau;
SET desKkEra = desSopEur / (inSopKau * 12);
SET i = (inSopKau * 12) / inLasKau;

while count <= i do
	set count = count + 1;
	INSERT INTO kk_erat (tarjous_ID009, pvm, kk_era)
	VALUES (idTarjous, inSopAlkaa, desKkEra);
	set inSopAlkaa = DATE_ADD(inSopAlkaa, INTERVAL inLasKau MONTH);
end while;

SET status_out = TRUE;
END*/