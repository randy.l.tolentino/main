/*INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (45,1,'Huolto','S-huolto','60000','120000',406.67,0.00,0.00,1,1)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (45,1,'Huolto','M-huolto','120000','120000',582.43,0.00,0.00,1,1)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (45,1,'Huolto','L-Huolto','0','0',953.76,0.00,0.00,2,0)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (45,3,'Huolto','Väliöljynvaihto','','',295.24,0.00,0.00,2,0)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (45,4,'Huollon lisätyö','Jos on Haldex-kuivain (per M-huolto)','','',54.34,0.00,0.00,2,0)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (45,5,'Huollon lisätyö','Vetopöydän rasvaus (per kerta)','','',20.00,0.00,0.00,2,0)^;

INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (10,1,'Huolto','Perushuolto','60000','120000',410.30,0.00,0.00,1,1)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (10,2,'Huolto','Vuosihuolto','120000','120000',917.77,0.00,0.00,1,1)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (10,3,'Huolto','Väliöljynvaihto','','',187.44,0.00,0.00,2,0)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (10,5,'Huollon lisätyö','Vetopöydän rasvaus (per kerta)','','',20.00,0.00,0.00,2,0)^;

INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (12,1,'Huolto','Perushuolto','60000','120000',363.87,0.00,0.00,1,1)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (12,2,'Huolto','Vuosihuolto','120000','120000',1010.63,0.00,0.00,1,1)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (12,3,'Huolto','Väliöljynvaihto','','',206.01,0.00,0.00,2,0)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (12,5,'Huollon lisätyö','Vetopöydän rasvaus (per kerta)','','',20.00,0.00,0.00,2,0)^;

INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (19,1,'Huolto','Perushuolto','60000','120000',519.77,0.00,0.00,1,1)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (19,2,'Huolto','Vuosihuolto','120000','120000',1200.69,0.00,0.00,1,1)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (19,3,'Huolto','Väliöljynvaihto','','',315.49,0.00,0.00,2,0)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (19,5,'Huollon lisätyö','Vetopöydän rasvaus (per kerta)','','',20.00,0.00,0.00,2,0)^;

INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (NULL,10,'Rasvaus','Perävaunun rasvaus huollon yhteydessä','','',62.50,0.00,0.00,3,0)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (NULL,11,'Katsastus','Kuorma-auto','','',414.00,0.00,0.00,3,0)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (NULL,12,'Katsastus','Perävaunu','','',239.00,0.00,0.00,3,0)^;
INSERT INTO mallikohtaiset_rivit (MALLI_ID002,Lajittelu,Tyyppi,Selite,Eka_Huolto_Km,Normi_Huolto_Vali,Hinta,kpl,Summa,Vakio,Vahintaan_kpl) VALUES (NULL,13,'Katsastus','Perävaunu + Dolly','','',435.00,0.00,0.00,3,0)^;
*/

/*MERKKI and MALLI
insert into merkki(merkki,erityisehdot,hinnasto) values ('merki1','merki2','merki3');
insert into malli(merkki_id001,malli,huoltovali,huolto_ohjelma,km,kustannukset,kk,snt,info) values(1,'malli1',1,'malli1',1,1.1,1.2,1.3,'malli1');

TARJOUS
insert into tarjous (merkki_id001, id002 , id012 , id020 , kk , ajotehtava , alennuksen_syyajotehtava , alennus_p , amw_info , asiakasnro , asiakkaan_nimi , emai_asiakas , email_hs , email_paattynyt , erityisehdot , hinnasto , huolto_ohjelma , info , keskeytyksen_syy , keskeytys_pvm       , km , km_sop_alkaa , km_sop_paattyy , kustannukset , laskutus_postinumero_ja_toimipaikka , laskutus_tapahtuu , laskutusosoite , liitetiedostot , lisaehdot , luotu               , muistio , muokattu            , muuta , nav_sopimusnro , osoite , postinumero_ja_toimipaikka , puhelinnumero , rek_luku , rek_nro , rek_nro_perakarry , rekisterointi_pvm   , sahkoposti_osoite , snt , sopimus_alkaa       , sopimus_paattyy     , sopimuskausi , tarjouksen_tekija , tilakoodi)
select id001, id002 , id012 , id020 , kk , ajotehtava , alennuksen_syyajotehtava , alennus_p , amw_info , asiakasnro , asiakkaan_nimi , emai_asiakas , email_hs , email_paattynyt , erityisehdot , hinnasto , huolto_ohjelma , info , keskeytyksen_syy , keskeytys_pvm       , km , km_sop_alkaa , km_sop_paattyy , kustannukset , laskutus_postinumero_ja_toimipaikka , laskutus_tapahtuu , laskutusosoite , liitetiedostot , lisaehdot , luotu               , muistio , muokattu            , muuta , nav_sopimusnro , osoite , postinumero_ja_toimipaikka , puhelinnumero , rek_luku , rek_nro , rek_nro_perakarry , rekisterointi_pvm   , sahkoposti_osoite , snt , sopimus_alkaa       , sopimus_paattyy     , sopimuskausi , tarjouksen_tekija , tilakoodi from offer;

insert into toimipiste(toimipiste, tilakoodi)
values('Espoo','Käytössä'),
('Hämeenlinna','Käytössä'),
('Joensuu','Käytössä'),
('Jyväskylä','Käytössä'),
('Kajaani','Käytössä'),
('Kotka','Käytössä'),
('Kouvola','Käytössä'),
('Kuopio','Käytössä'),
('Lahti','Käytössä'),
('Lappeenranta','Käytössä'),
('Mikkeli','Käytössä'),
('Oulu','Käytössä'),
('Rovaniemi','Käytössä'),
('Seinäjoki','Käytössä'),
('Tampere','Käytössä'),
('Turku','Käytössä'),
('Vaasa','Käytössä'),
('Vantaa Airport','Käytössä'),
('Ylivieska','Käytössä');*/

